#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
OSD Neo2
========
On screen display for learning the keyboard layout Neo2

Copyright (c) 2009-2010 Martin Zuther (http://www.mzuther.de/)
Copyright (c) 2015-2022 Hartmut Goebel (http://crazy-compilers.com/)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Thank you for using free software!

"""

import os
from optparse import OptionParser


from osd_neo2.locale import _
from osd_neo2.settings import *


# check whether the script runs with superuser rights
if (os.getuid() == 0) or (os.getgid() == 0):
    print(_('For security reasons you may not run this application with superuser rights.'))


# initialise version information, ...
version_long = (_('%(description)s\n%(copyrights)s\n\n%(license)s') %
                {'description': settings.get_description(True),
                 'copyrights': settings.get_copyrights(),
                 'license': settings.get_license(True)})
# ... usage information and, ...
usage = ('Usage: %(cmd_line)s [options]' %
         {'cmd_line': settings.get_variable('cmd_line')})
# ... the command line parser itself
parser = OptionParser(usage=usage, version=version_long)

# parse command line
(options, args) = parser.parse_args()

import osd_neo2.app  # noqa E402 - init Gtk late
base = osd_neo2.app.OSDneo2()
base.main()
